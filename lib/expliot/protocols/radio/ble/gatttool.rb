
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

module Expliot

    module Protocols

        module Radio

            module BLE

                class GATTTool

                    CMD = "gatttool"

                    #
                    # Primary Service Discovery
                    #
                    # @param bdaddr BLE Device Address
                    # @param iface  BLE adapter interface to use
                    #
                    # @return Array containing output of gatttool 
                    #
                    def self.scan_primary(bdaddr, iface = "hci0")
                        rarray = []
                        cmd = [CMD]

                        cmd.push("--adapter=#{iface}")
                        cmd.push("--device=#{bdaddr}")
                        cmd.push("--primary")
                        proc = IO.popen(cmd)
                        rarray = proc.readlines
                        proc.close()
                        return rarray
                    end

                    #
                    # Characteristics Discovery
                    #
                    # @param bdaddr BLE Device Address
                    # @param iface  BLE adapter interface to use
                    #
                    # @return Array containing output of gatttool 
                    #
                    def self.scan_characteristics(bdaddr, iface = "hci0")
                        rarray = []
                        cmd = [CMD]

                        cmd.push("--adapter=#{iface}")
                        cmd.push("--device=#{bdaddr}")
                        cmd.push("--characteristics")
                        proc = IO.popen(cmd)
                        rarray = proc.readlines
                        proc.close()
                        return rarray
                    end

                    #
                    # Characteristic value Read Write 
                    #
                    # @param bdaddr BLE Device Address
                    # @param chcmd  Characteristic read/write command. one of
                    #               :char_read, :char_write or :char_write_req
                    # @param handle Handle required for read/write
                    # @param value  Value to write. Used only for write
                    # @param iface  BLE adapter interface to use. Default is hci0
                    #
                    # @return Array containing output of gatttool
                    #
                    def self.char_read_write(bdaddr, chcmd, handle, value, iface = "hci0")
                        rarray = []
                        cmd = [CMD]

                        cmd.push("--adapter=#{iface}")
                        cmd.push("--device=#{bdaddr}")
                        case chcmd
                        when :char_read
                            cmd.push("--char-read")
                        when :char_write
                            cmd.push("--char-write")
                            cmd.push("--value #{value}")
                        when :char_write_req
                            cmd.push("--char-write-req")
                            cmd.push("--value #{value}")
                        else
                            raise ArgumentError.new("Invalid charecteristic read/write command: #{cmd}")
                        end
                        cmd.push("--handle #{handle}")
                        # If cmd is passed as an array then we get the error - Unknown option --value d0fffffff
                        # where d0fffffff is the value passed to this method
                        proc = IO.popen(cmd.join(' '))
                        rarray = proc.readlines
                        proc.close()
                        #puts "#{cmd.join(' ')}"
                        return rarray
                    end

                end
            end
        end
    end
end

#Expliot::Protocols::Radio::BLE::GATTTool.char_read_write("01:02:03:04:05:06", :char_write, "0x002b", "d0fffffff")