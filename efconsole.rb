#!/usr/bin/env ruby
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

libdir = File.join(File.expand_path(File.dirname(__FILE__)), "lib")
$:.unshift(File.expand_path(libdir)) unless
    $:.include?(libdir) || $:.include?(File.expand_path(libdir))

require 'readline'
require 'cri'
require 'expliot'
require 'plugins'

module EFConsole

    PROMPT = "ef>"
    
    def self.main
        efcmd = Expliot::Commands.init_commands(PROMPT)
        intro()
        while true
            begin
                cmd = Readline.readline(PROMPT, true)
                if cmd.empty? then
                    Readline::HISTORY.pop
                else
                    efcmd.run(cmd.split, hard_exit: false)
                end
            rescue ArgumentError => e
                puts "ArgumentError msg = (#{e.message})"
            rescue StandardError => ex
                puts "Oopsy! Error (#{ex.message}) (#{ex.class})"
            end
        end
    end
end

# XXX Create Version and other config yaml file instead of hardcoding it
def intro
    puts ""
    puts ""
    puts "                  __   __      _ _       _"    
    puts "                  \\ \\ / /     | (_)     | |"  
    puts "               ___ \\ V / _ __ | |_  ___ | |_"
    puts "              / _ \\/   \\| '_ \\| | |/ _ \\| __|" 
    puts "              | __/ /^\\ \\ |_) | | | (_) | |_"
    puts "              \\___\\/   \\/ .__/|_|_|\\___/ \\__|" 
    puts "                         | |"                   
    puts "                         |_|"
    puts ""
    puts ""
    puts "                         expliot"
    puts "                      version #{Expliot::VERSION}"
    puts ""
    puts "                    Internet Of Things"
    puts "             Security Testing and Exploitation" 
    puts "                        Framework"
    puts "" 
    puts "                     By Aseem Jakhar"
    puts ""
    puts ""
end

EFConsole.main

